const gotCitiesCSV = ["King's Landing","Braavos","Volantis","Old Valyria","Free Cities","Qarth","Meereen"]
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function kata1(n){
    for (let i = 0; i<n; i++){
        let hold = gotCitiesCSV[i]
        addKata(hold)
    }
}

function kata2(n){
    for (let j = 0; j<n; j++){
         let store = lotrCitiesArray[j]
        addKata(store)
    }
}

function kata3(n){
    if (n===n){
    addKata(gotCitiesCSV.join(" ; "));
    }
}

function kata4(n){
    if (n===n){
    addKata(lotrCitiesArray.join(", "));
    }
}
function addQ(text) {
    const div = document.createElement("div")
    div.textContent = text
    document.querySelector("body").appendChild(div)
}
function addKata(stuff) {
    const div = document.createElement("div")
    div.textContent = stuff
    document.querySelector("body").appendChild(div)
}

function kata5(arr){
    a=[]
    for(let i=0; i < 5; i++){
        hold = arr[i];
        a.push(hold)
        
    }
    addQ(a)
}


function kata6(arr){
    a=[]
    for(let i=0; i < 5; i++){
        hold = arr[arr.length - 1 - i];
        a.push(hold)
        
    }
    addQ(a)
}
function kata7(arr){
    for(let i=2; i < 5; i++){
        hold = lotrCitiesArray[i];
        arr.push(hold)
        
    }
    addQ(arr)
}

function kata8(arr){
    arr = lotrCitiesArray.splice(2,1)
    addQ(lotrCitiesArray)
}


function kata10(arr){
    lotrCitiesArray.splice(2,0, "Rohan")
    addQ(lotrCitiesArray)
}

function kata11(arr){
    
    lotrCitiesArray.splice(5,1, "Deadest Marshes")
    addQ(lotrCitiesArray)
}
function kata9(arr){
    arr = lotrCitiesArray.splice(5,2)
    addQ(lotrCitiesArray)
}

function kata12(thingstring){
    str2arr = thingstring.split("")
    addQ(str2arr.slice(0,14))

}

function kata13(thingstring){
    str2arr = thingstring.split("")
    addQ(str2arr.slice(69))
}


function kata14(thingstring){
    str2arr = thingstring.split("")
    addQ(str2arr.slice(23,38))
}

function kata15(thingstring){
    addQ(thingstring.substring(69,81))
}

function kata16(thingstring){
    addQ(thingstring.substring(23,38))
}
function kata17(thingstring){
    str2arr= thingstring.split(" ");
    addQ(str2arr.indexOf('only'))
}

function kata18(thingstring){
    str2arr = thingstring.split(" ")
    addQ(str2arr.indexOf(str2arr[str2arr.length - 1]))
}

function kata19(arr){
    arr.forEach(hold => {if(hold.includes("ee") || hold.includes("aa")){addQ(hold)}})
}

function kata20(arr){
    arr.forEach(hold => {if(hold.includes("or")){addQ(hold)}})}

function kata21(thingstring){
    str2arr = thingstring.split(" ")
    arr = str2arr.splice(3,1)
    str2arr.forEach(hold => {if(hold.includes("b")){addQ(hold)}})
}

function kata22(arr){
   let compare = "Mirkwood";
   let check = compare.split("");
   let result = 0;
   let loopended = 0;
   for(let i=0; i<arr.length;i++){
       hold = lotrCitiesArray[i].split("")
       for (let j = 0; j< hold.length; j++){
           if(hold[j]===check[j]){
               continue
           }
           else{
               loopended = loopended + 1
               break
           }
       }
       if(loopended === 0){
           addQ("yes");
           break;
       }
       else{
           loopended = 0;
           result = result + 1
           if(result === arr.length){
               addQ("no")
           }
           continue
       }
       
   }
}

function kata23(arr){
    let compare = "Hollywood";
    let check = compare.split("");
    let result = 0;
    let loopended = 0;
    for(let i=0; i<arr.length;i++){
        hold = lotrCitiesArray[i].split("")
        for (let j = 0; j< hold.length; j++){
            if(hold[j]===check[j]){
                continue
            }
            else{
                loopended = loopended + 1
                break
            }
        }
        if(loopended === 0){
            addQ("yes");
            break;
        }
        else{
            loopended = 0;
            result = result + 1
            if(result === arr.length){
                addQ("no")
            }
            continue
        }
        
    }
 }

 function kata24(arr){
     addQ(arr.indexOf('Mirkwood'))
 }

 function kata25(arr){
     let cond=0;
    for(let i=0; i<arr.length;i++){
      hold = lotrCitiesArray[i].split("")
      for (let j = 0; j< hold.length; j++){
          if(hold[j]=== " "){
              addQ(arr[i])
              cond=cond+1
              break
          }
          else{
              continue
          }
      }
      if (cond===1){
          break
      }
      else if (i===arr.length-1){
          addQ("there are no cities with two words")
      }
      else{
          continue
      }
 }
}

function kata26(arr){
    let rev = [];
    for(let i=arr.length-1; i>= 0; i--){
        rev.push(arr[i])
    }
    addQ(rev)
}

function kata27(arr){
    addQ(arr.sort());
}

addQ("1.) Write a function that returns an array with the cities in 'gotCitiesCSV'")
kata1(gotCitiesCSV.length)

addQ("2.) Write a function that returns an array of words from the sentence in 'bestThing'")
kata2(lotrCitiesArray.length)

addQ("3.) Write a function that returns a string separated by semi-colons instead of commas from 'gotCitiesCSV'")
kata3(1)

addQ("4.) Write a function that returns a CSV (comma-separated) string from the 'lotrCitiesArray'")
kata4(1)

addQ("5.) Write a function that returns an array with the first 5 cities in 'lotrCitiesArray'.")
kata5(lotrCitiesArray)

addQ("6.) Write a function that returns an array with the last 5 cities in 'lotrCitiesArray'")
kata6(lotrCitiesArray)

addQ("7.) Write a function that returns an array with the 3rd to 5th city in 'lotrCitiesArray'")
let midC = []
kata7(midC)

addQ("8.) Write a function that uses 'splice' to remove 'Rohan' from 'lotrCitiesArray' and returns the new modified 'lotrCitiesArray")
let smallC = []
kata8(smallC)

addQ("9.) Write a function that uses 'splice' to remove all cities after 'Dead Marshes' in 'lotrCitiesArray' and returns the new modified 'lotrCitiesArray'")
let takelast2
kata9(takelast2)

addQ("10.) Write a function that uses 'splice' to add 'Rohan' back to 'lotrCitiesArray' right after 'Gondor' and returns the new modified 'lotrCitiesArray'. ")
let in_out = []
kata10(in_out)

addQ("11.)Write a function that uses 'splice' to rename 'Dead Marshes' to 'Deadest Marshes' in 'lotrCitiesArray' and returns the new modified 'lotrCitiesArray'")
let dead2est = []
kata11(dead2est)

addQ("12.)Write a function that uses 'slice' to return a string with the first 14 characters from 'bestThing'")
kata12(bestThing)

addQ("13.) Write a function that uses 'slice' to return a string with the last 12 characters from 'bestThing'")
kata13(bestThing)

addQ("14.) Write a function that uses 'slice' to return a string with the characters between the 23rd and 38th position of 'bestThing' (i.e., 'boolean is even')")
kata14(bestThing)

addQ("15.) Write a function that does the exact same thing as #13 but use the 'substring' method instead of 'slice'")
kata15(bestThing)

addQ("16.) Write a function that does the exact same thing as #14 but use the 'substring' method instead of 'slice'. ")
kata16(bestThing)

addQ("17.) Write a function that finds and returns the index of 'only' in 'bestThing'")
kata17(bestThing)

addQ("18.) Write a function that finds and returns the index of the last word in 'bestThing'")
kata18(bestThing)

addQ("19.) Write a function that finds and returns an array of all cities from 'gotCitiesCSV' that use double vowels ('aa', 'ee', etc)")
kata19(gotCitiesCSV)

addQ("20.) Write a function that finds and returns an array with all cities from 'lotrCitiesArray' that end with 'or' ")
kata20(lotrCitiesArray)

addQ("21.) Write a function that finds and returns an array with all the words in 'bestThing' that start with a 'b'")
kata21(bestThing)

addQ("22.) Write a function that returns 'Yes' or 'No' if 'lotrCitiesArray' includes 'Mirkwood'")
kata22(lotrCitiesArray)

addQ("23.) Write a function that returns 'Yes' or 'No' if 'lotrCitiesArray' includes 'Hollywood'")
kata23(lotrCitiesArray)

addQ("24.) Write a function that returns the index of 'Mirkwood' in 'lotrCitiesArray'")
kata24(lotrCitiesArray)

addQ("25.) Write a function that finds and returns the first city in 'lotrCitiesArray' that has more than one word. ")
kata25(lotrCitiesArray)

addQ("26.) Write a function that reverses the order of 'lotrCitiesArray' and returns the new array")
kata26(lotrCitiesArray)

addQ("27.) Write a function that sorts 'lotrCitiesArray' alphabetically and returns the new array")
kata27(lotrCitiesArray)